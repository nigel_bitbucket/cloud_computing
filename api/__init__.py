from flask_restful import Api
from app import flaskAppInstance
from .TaskAPI import Task
from .TaskByID2API import TaskByID
from .TaskByID2API import TaskByID1
from .TaskByID2API import TaskByID2
from .TaskByID2API import TaskByID3

restServerInstance = Api(flaskAppInstance)

restServerInstance.add_resource(Task, "/api/v1.0/sat")
restServerInstance.add_resource(TaskByID, "/api/v1.0/sat/<string:satId>")
restServerInstance.add_resource(TaskByID1, "/api/v1.0/sat/decommission/<string:satId>")
restServerInstance.add_resource(TaskByID2, "/api/v1.0/sat/description/<string:satId>")
restServerInstance.add_resource(TaskByID3, "/api/v1.0/sat/alias/<string:satId>")
