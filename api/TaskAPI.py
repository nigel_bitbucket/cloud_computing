from flask_restful import Resource
from flask import request
import logging as logger
import db
import json


class Task(Resource):

    def get(self):
        db.cur.execute("select * from satellite")

        result = db.cur.fetchall()
#       db.conn.close()
        # return the results!
        logger.debug("Inside get method of Task")
        return json.dumps(result), 200

    def post(self):
        satid = request.json['id']
        alias = request.json['alias']
        description = request.json['description']
        status = request.json['status']
        locationid = request.json['locationid']
        launchsite = request.json['launchsite']
        owner = request.json['owner']

        sql = "insert into satellite values (%s,%s,%s,%s,%s,%s,%s)"
        db.cur.execute(sql, (satid, alias, description, status, locationid, launchsite, owner))

        db.conn.commit()
        logger.debug("Inisde the post method of Task")
        return {"message": "Added New Satellite - {}".format(alias)}, 200